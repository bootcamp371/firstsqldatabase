﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstDatabaseConnection
{
    public class ConnectionString
    {
        public static string? GetConnectionString()
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false,
            reloadOnChange: true);
            IConfiguration config = builder.Build();
            return config["ConnectionStrings:Northwind"];
        }

    }
    
}
