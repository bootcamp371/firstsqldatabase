﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.IO;

namespace FirstDatabaseConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
           
            string? connStr = ConnectionString.GetConnectionString();
                
            if (connStr != null)
            {
                Console.WriteLine(connStr);
            }
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            //Console.WriteLine("Success");
            int catCount = DisplayCategoryCount(cn);
            Console.WriteLine("The category Count is : " + catCount);

            decimal productsTotal = DisplayTotalInventoryValue(cn);
            Console.WriteLine("The inventory total is: " + productsTotal);
            //AddAShipper(cn);
            //ChangeShipperName(cn);
            //DeleteShipper(cn);
            //DisplayAllProducts(cn);
            //DisplayProductsInCategory(cn);
            //DisplayProductsForSupplier(cn);
            //DisplaySupplierProductCounts(cn);
            //DisplayCustomersandSuppliersByCity(cn);
            //DisplayProductsForSupplierParameterized(cn, "Produce", "Plutzer Lebensmittelgroßmärkte AG");
            //DisplayTenMostExpensiveProducts(cn);
            DisplaySalesByCategory(cn);
            cn.Close(); 
        }

        public static void DisplaySalesByCategory(SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("SalesByCategory", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryName", "Beverages");
            cmd.Parameters.AddWithValue("@OrdYear", "1997");
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    string productName = (string)dr["ProductName"];
                    decimal totalPurchase = (decimal)dr["TotalPurchase"];
                    Console.WriteLine($"{productName} -- {totalPurchase:C}");
                }
            }

        }
        public static void DisplayTenMostExpensiveProducts(SqlConnection cn)
        {
            SqlCommand cmd = new SqlCommand("Ten Most Expensive Products", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    string productName = (string)dr["TenMostExpensiveProducts"];
                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    Console.WriteLine($"{productName} -- {unitPrice:C}");
                }
            }
        }
        public static void DisplayProductsForSupplierParameterized(SqlConnection cn, string categoryName, string supplierName)
        {
            try
            {
                /*cn.Open();*/
                string sql = "Select p.ProductID,   p.ProductName,   p.UnitsInStock,   p.UnitPrice From Products p "
                    + "Inner join Categories c On p.CategoryID = c.CategoryID Inner Join Suppliers s On p.SupplierID = s.SupplierID Where "
                    /*+ "c.CategoryName = '"
                    + categoryName + "' AND s.CompanyName = '" + supplierName + "'"*/
                    + "categoryName = @categoryName AND s.CompanyName = @supplierName"
                    ;
                SqlParameter param = new SqlParameter("@categoryName", categoryName);
                param.Value = categoryName;
                SqlParameter param2 = new SqlParameter("@supplierName", supplierName);
                param2.Value = supplierName;
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param2);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int productID = (int)dr["ProductID"];
                            string productName = (string)dr["ProductName"];
                            Int16 unitsInStock = (Int16)dr["UnitsInStock"];
                            decimal unitPrice = (decimal)dr["UnitPrice"];
                            Console.WriteLine(
                            $"{supplierName}  +  {categoryName} :  [{productID}] {productName} - {unitsInStock} - {unitPrice:C}");
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
            finally
            {
                /*cn.Close();*/
            }
        }

        public static void DisplayCustomersandSuppliersByCity(SqlConnection cn)
        {
            string sql = "Select City, CompanyName, ContactName from [Customer and suppliers by city]";
            SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    string companyName = (string)dr["CompanyName"];
                    string city = (string)dr["City"];
                    string contact = (string)dr["ContactName"];
                    Console.WriteLine($"CompanyName: {companyName} City: {city} Contact: {contact}");
                }

            }
        }

        public static void DisplaySupplierProductCounts(SqlConnection cn)
        {
            string sql = "Select suppliers.companyName, count(products.supplierId) as numberOfItems from products " +
                            "join suppliers on products.supplierID = suppliers.supplierID " +
                            "Group by suppliers.CompanyName";
            SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    string companyName = (string)dr["companyName"];
                    int numItems = (int)dr["numberOfItems"];

                    Console.WriteLine($"Company Name: {companyName} Number of items for sale: {numItems}");
                }

            }
        }
        public static void DisplayProductsForSupplier(SqlConnection cn)
        {
            string sql = "Select p.productId, p.productName, p.unitsInStock, " +
                            "p.unitPrice, s.companyName, c.categoryName " +
                            "from suppliers s " +
                            "inner join products p on s.supplierId = p.supplierId " +
                            "inner join categories c on p.categoryId = c.categoryId " +
                            "where s.companyName = 'Bigfoot Breweries'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    int productID = (int)dr["productId"];
                    // or productID = Convert.ToInt32(dr["ProductID"])
                    string productName = (string)dr["ProductName"];
                    // or productName = dr["ProductName"].ToString();
                    int qtyOnHand = (int)(Int16)dr["UnitsInStock"];
                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    string categoryName = (string)dr["categoryName"];
                    string supplierName = (string)dr["companyName"];
                    // or unitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    Console.WriteLine($"Company Name: {supplierName} [{productID}] Name: {productName} Category: {categoryName} Price: {unitPrice:C} Qty Oh Hand: {qtyOnHand}");
                }

            }
        }
        public static void DisplayProductsInCategory(SqlConnection cn)
        {
            string sql = "Select products.categoryid, products.productName, products.unitsinstock, products.unitprice from products " +
                            "Inner join categories on categories.categoryid = products.categoryid where categories.categoryname = 'produce'";
            SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    int categoryID = (int)dr["categoryID"];
                    // or productID = Convert.ToInt32(dr["ProductID"])
                    string productName = (string)dr["ProductName"];
                    // or productName = dr["ProductName"].ToString();
                    int qtyOnHand = (int)(Int16)dr["UnitsInStock"];
                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    // or unitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    Console.WriteLine($"[{categoryID}] Name: {productName} Price: {unitPrice:C} Qty Oh Hand: {qtyOnHand}");
                }

            }
        }
        public static void DisplayAllProducts(SqlConnection cn)
        {
            string sql = "Select productId, ProductName, UnitsInStock, unitPrice from Products";
            SqlCommand cmd = new SqlCommand(sql, cn);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    int productID = (int)dr["ProductID"];
                    // or productID = Convert.ToInt32(dr["ProductID"])
                    string productName = (string)dr["ProductName"];
                    // or productName = dr["ProductName"].ToString();
                    int qtyOnHand = (int)(Int16)dr["UnitsInStock"];
                    decimal unitPrice = (decimal)dr["UnitPrice"];
                    // or unitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    Console.WriteLine($"[{productID}] Name: {productName} Price: {unitPrice:C} Qty Oh Hand: {qtyOnHand}");
                }

            }
        }

        public static int DisplayCategoryCount(SqlConnection cn)
        {
            
            string sql = "Select Count(*) From Products";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int catCount = (int)cmd.ExecuteScalar();
            return catCount;
        }

        public static decimal DisplayTotalInventoryValue(SqlConnection cn)
        {
            
            string sql = "SELECT Sum(UnitPrice* UnitsInStock) as Result FROM Products";
            SqlCommand cmd = new SqlCommand(sql, cn);
            decimal productsTable = (decimal)cmd.ExecuteScalar();
            return productsTable;
        }

        public static void AddAShipper(SqlConnection cn)
        {
            string sql = "Insert into Shippers values('Federal Express','248-840-0458')";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int insertShipper = (int)cmd.ExecuteNonQuery();
            Console.WriteLine("Insert shipper:" + insertShipper);
        }

        public static void ChangeShipperName(SqlConnection cn)
        {
            string sql = "Update Shippers Set CompanyName = 'Marks shipping' " +
                "Where ShipperID = 4";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int updateName = (int)cmd.ExecuteNonQuery();
            
        }
        public static void DeleteShipper(SqlConnection cn)
        {
            string sql = "Delete From  Shippers Where ShipperID = 4";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int deleteShipper = (int)cmd.ExecuteNonQuery();
        }
    }
}
